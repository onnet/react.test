import { applyMiddleware, createStore } from 'redux';
import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';

// Reducers
import reducer from './reducers';

const middleware = applyMiddleware(promise(), thunk);

export default createStore(reducer, middleware);
