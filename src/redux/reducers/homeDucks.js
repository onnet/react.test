const initialState = {
    // Inital state variables to go here.
};

// Reducer
export default function reducer(state = initialState, action) {
    switch (action.type) {
        default: {
            return state;
        }
    }
}

// Action creators to go here.
