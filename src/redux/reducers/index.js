import { combineReducers } from 'redux';

import homeReducer from './homeDucks';

export default combineReducers({
    homeReducer
});
