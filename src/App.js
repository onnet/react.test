import React from 'react';
import Home from './features/home/Home';

require('./App.scss');

const App = () => {
    return (
        <div className="App">
            <Home />
        </div>
    );
};

export default App;
