import React from 'react';

// Styles
require('./styles.scss');

const defaultProps = {
    // default props to go here.
};

const propTypes = {
    // Prop Types to go here.
};

const Home = () => {
    return (
        <div>
            Welcome to the Hyve React Test
        </div>
    );
};

Home.defaultProps = defaultProps;
Home.propTypes = propTypes;

export default Home;
