import React from 'react';

const TestComponent = () => {
    return (
        <div>
            Component to go here.
        </div>
    );
};

export default TestComponent;
