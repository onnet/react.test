## Hyve React Test Project

### Goal

Create a React application that uses the Reddit API to pull a subreddit, and displays each image of the subreddit. 

* eslint to be adhered to. 

* Redux to be used for state management. 

* Handle all potential errors that may occur in a graceful manner 

* Maintain a clean codebase.

### Instructions
* Clone this repo.

* Initialize git flow.

* Create a feature branch with your solution. `feature/YourName`

* Once complete, create a pull request.

